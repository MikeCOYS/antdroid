package antgame;
import java.util.ArrayList;


public class WorldParser {
	
	public String rawWorld;
	public int width;
	public int height;
	private ArrayList<String> rows;
	private ArrayList<String[]> cells;

	public WorldParser(String world) throws WorldException{
		rawWorld = world;
		parseWorld();
	}
	
	private void parseWorld() throws WorldException {
		//separate the lines from the source file by new line character and put them in an array
		String[] lines = rawWorld.split("\\n");
		try {
			//parse the first line to be width
			width = Integer.parseInt(lines[0]);
		} catch (NumberFormatException nfe) {
			System.out.println("Width is not a number! " + nfe);
			throw new WorldException("Width is not a number!");
		}
		try {
			//parse the second line to be height
			height = Integer.parseInt(lines[1]);
		} catch (NumberFormatException nfe) {
			System.out.println("Height is not a number! " + nfe);
			throw new WorldException("Height is not a number!");
		}
		
		//add all other lines into the rows array
		rows = new ArrayList<String>();
		for (int i = 2; i < lines.length; i++) {
			rows.add(lines[i]);
		}
		
		//check if the number of lines matches the height
		if (rows.size() != height) {
			throw new WorldException(
					"Number of rows does not match the declared height");
		} else {

			cells = new ArrayList<String[]>();	//initialise ArrayList
			
			//iterate though rows and split them into cells by whitespace
			//store each line of cells in String[] rowCells array
			//store rows (lines) in ArrayList<String[]> cells array 
			
			int i = 1; //keep track of row id being scanned

			for (String row : rows) {
				String[] rowCells = row.split("\u0020");	//split by whitespace
				if (rowCells.length != width) {
					throw new WorldException("Number of cells in row " + i
							+ " does not match the specified width");
				} else {
					cells.add(rowCells);
				}
				i++;
			}

		}

	}
	public ArrayList<String[]> getCells(){
		return cells;
	}
}
