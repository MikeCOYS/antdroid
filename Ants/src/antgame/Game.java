package antgame;
import java.io.FileNotFoundException;
import java.util.ArrayList;
//import java.io.IOException;
//import java.util.ArrayList;

public class Game {
	
	/**
	 * The main class for starting the game and testing.
	 */
	//private Match currentMatch;
	private ArrayList<Match> matchList;
	
	public static void main(String[] args) {
				
		Game game = new Game();
		
		try{
			Match match = game.createMatch("solution-1.brain", "cleverbrain1.brain", "a.world");
			game.addMatch(match);
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
		
		

	}

	public Game() {
		
		matchList = new ArrayList<Match>();
		System.out.println("New game created!");
		
	}
	private void addMatch (Match m){
		matchList.add(m);
		//System.out.println("Match added! Info:");
		//printInfo(matchList.get(matchList.size()-1));
		
	}
	private Match createMatch (String redBrainName, String blackBrainName, String worldName) throws Exception {
		
		Match myMatch = new Match(10000);
		try {
			
			//myMatch.addAntBrain(redBrainName,"red");
			//myMatch.addAntBrain(blackBrainName, "black");
			myMatch.addWorld(worldName);

		}catch (FileNotFoundException fnfe){
			System.out.println(fnfe);
			throw new Exception ("game exception: "+fnfe.getLocalizedMessage());
		}catch(MatchException me){
			System.out.println(me);
			throw new Exception ("game exception: "+me.getLocalizedMessage());

		}
		
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return myMatch;
		
	}
	
	private void printInfo(Match m){
		System.out.println("Red brain set: "+m.redAntBrainSet+" "+m.redAntBrainName);
		System.out.println("Black brain set: "+m.blackAntBrainSet+" "+m.blackAntBrainName);
		System.out.println("World set: "+m.worldSet+" "+m.worldName);
		System.out.println("b lines: "+m.blackAntBrain.getInstructions().size());
		System.out.println("r lines: "+m.redAntBrain.getInstructions().size());
		System.out.println("w dimensions: "+m.world.getHeight()+"/"+m.world.getWidth());
		System.out.println("Match round: "+m.getRound());
	}
	
	

}
