package antgame;

public class WorldGenerator {
	
	/**
	 * WorldGenerator class is used to generate syntactically valid world.
	 * Takes into account the width and height of the world, amount of food,
	 * width of height of food blobs, length of hexagonal anthills and number of rocks in the world.
	 * 
	 * To implement.
	 * 
	 */
	public WorldGenerator(){
		
	}
	public void generate(int width, int height, int food, int foodWidth, int foodHeight, int rocks, int antHillHexLength){
		//to implement and return a valid world
	}
}
