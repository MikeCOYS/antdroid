package antgame;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

public class BrainParser {

	private ArrayList<Instruction> iList;
	private int length = 0;

	public BrainParser(String brain, int maxStates) throws AntBrainException {
		Lexer lexer = new Lexer(brain);
		ArrayList<ArrayList<Token>> tokenLines = lexer.getTokenLines();

		length = tokenLines.size()-1;
		if (length > maxStates) {
			throw new AntBrainException("Brain has more than max of "
					+ maxStates + " lines");
		}
		iList = new ArrayList<Instruction>();

		for (int i = 0; i < tokenLines.size(); i++) {
			ArrayList<Token> line = tokenLines.get(i);
			try {
				parseLine(line);
			} catch (AntBrainException e) {
				throw new AntBrainException("error at line " + i + "("+line+ ": "
						+ e.getLocalizedMessage());
			}
		}
	}



	private void parseLine(ArrayList<Token> brainLine) throws AntBrainException {
		Token instr = brainLine.get(0);
		String s = "";

		Instruction i = new Instruction();
		int n = 0;	//scanned tokens in a line. used to find comments

		try {
			if (instr.equals("T_Sense")) {
				n++;
				s += "Sense ";
				i.type = IType.SENSE;
				ValueToken senseDir = (ValueToken) brainLine.get(1);
				String sensedir = senseDir.getValue();
				if (sensedir.equals("here")) {
					s += "Here ";
					i.sense_dir = SenseDir.HERE;

				} else if (sensedir.equals("ahead")) {
					s += "Ahead ";
					i.sense_dir = SenseDir.AREAD;

				} else if (sensedir.equals("leftahead")) {
					s += "LeftAhead ";
					i.sense_dir = SenseDir.LEFT_AHEAD;

				} else if (sensedir.equals("rightahead")) {
					s += "RightAhead ";
					i.sense_dir = SenseDir.RIGHT_AHEAD;
					
				} else {
					throw new AntBrainException(
							"unrecognised sense direction: " + senseDir);
				}
				n++;
				
				ValueToken state1 = (ValueToken) brainLine.get(2);
				int st1 = Integer.parseInt(state1.getValue());
				if (st1>length || st1<0){
					throw new AntBrainException("st1 should be between 0 and "+length);
				}else{
					s+=st1 +" ";
					i.st1 = st1;
				}
				n++;
				
				ValueToken state2 = (ValueToken) brainLine.get(3);
				int st2 = Integer.parseInt(state2.getValue());
				if (st2>length || st2<0){
					throw new AntBrainException("st2 should be between 0 and "+length);
				}else{
					s+=st2 +" ";
					i.st2 = st2;
				}
				n++;
				
				ValueToken condToken = (ValueToken) brainLine.get(4);
				String cond = condToken.getValue();
				if(cond.equals("friend")){
					s+="friend";
					i.cond = Condition.FRIEND;
				}else if(cond.equals("foe")){
					s+="foe";
					i.cond = Condition.FOE;
				}else if(cond.equals("marker")){
					s+="marker ";
					i.cond = Condition.MARKER;
					ValueToken markerToken = (ValueToken) brainLine.get(5);
					int m = Integer.parseInt(markerToken.getValue());
					if(m<0 || m>5){
						throw new AntBrainException ("friend marker must be between 0 and 5");
					}else{
						s+=m+"";
						i.markerId = m;
						n++;
					}
				}else if(cond.equals("home")){
					s+="home";
					i.cond = Condition.HOME;
				}else if(cond.equals("foehome")){
					s+="foehome";
					i.cond = Condition.FOE_HOME;
				}else if(cond.equals("foemarker")){
					s+="foemarker";
					i.cond = Condition.FOE_MARKER;
				}else if(cond.equals("rock")){
					s+="rock";
					i.cond = Condition.ROCK;
				}else if(cond.equals("food")){
					s+="food";
					i.cond = Condition.FOOD;
				}else if(cond.equals("foewithfood")){
					s+="foewithfood";
					i.cond = Condition.FOE_WITH_FOOD;
				}else if(cond.equals("friendwithfood")){
					s+="friendwithfood";
					i.cond = Condition.FRIEND_WITH_FOOD;
				}else{
					throw new AntBrainException ("Wrong condition: "+condToken);
				}
				n++;
				
				

			} else if (instr.equals("T_Mark")) {
				s+="Mark ";
				i.type = IType.MARK;
				n++;
				
				ValueToken iToken = (ValueToken) brainLine.get(n);
				int m = Integer.parseInt(iToken.getValue());
				if(m<0 || m>5){
					throw new AntBrainException ("Marker id must be between 0 and 5");
				}else{
					s+=m+" ";
					i.markerId = m;
					n++;
				}
				
				ValueToken state1 = (ValueToken) brainLine.get(n);
				int st1 = Integer.parseInt(state1.getValue());
				if (st1>length || st1<0){
					throw new AntBrainException("st1 should be between 0 and "+length);
				}else{
					s+=st1;
					i.st1 = st1;
				}
				n++;
				
				
			} else if (instr.equals("T_Unmark")) {
				s+="Unmark ";
				i.type = IType.UNMARK;
				n++;
				
				ValueToken iToken = (ValueToken) brainLine.get(n);
				int m = Integer.parseInt(iToken.getValue());
				if(m<0 || m>5){
					throw new AntBrainException ("Marker id must be between 0 and 5");
				}else{
					s+=m+" ";
					i.markerId = m;
					n++;
				}
				
				ValueToken state1 = (ValueToken) brainLine.get(n);
				int st1 = Integer.parseInt(state1.getValue());
				if (st1>length || st1<0){
					throw new AntBrainException("st1 should be between 0 and "+length);
				}else{
					s+=st1;
					i.st1 = st1;
				}
				n++;
				
			} else if (instr.equals("T_Pickup")) {
				s+="Pickup ";
				i.type = IType.PICKUP;
				n++;
				
				ValueToken state1 = (ValueToken) brainLine.get(n);
				int st1 = Integer.parseInt(state1.getValue());
				if (st1>length || st1<0){
					throw new AntBrainException("st1 should be between 0 and "+length);
				}else{
					s+=st1+" ";
					i.st1 = st1;
				}
				n++;
				
				ValueToken state2 = (ValueToken) brainLine.get(n);
				int st2 = Integer.parseInt(state2.getValue());
				if (st2>length || st2<0){
					throw new AntBrainException("st2 should be between 0 and "+length);
				}else{
					s+=st2;
					i.st2 = st2;
				}
				n++;

			} else if (instr.equals("T_Drop")) {
				s+="Drop ";
				i.type = IType.DROP;
				n++;
				
				ValueToken state1 = (ValueToken) brainLine.get(n);
				int st1 = Integer.parseInt(state1.getValue());
				if (st1>length || st1<0){
					throw new AntBrainException("st1 should be between 0 and "+length);
				}else{
					s+=st1;
					i.st1 = st1;
				}
				n++;

			} else if (instr.equals("T_Turn")) {
				s+="Turn ";
				i.type = IType.TURN;
				n++;
				
				ValueToken left_right = (ValueToken) brainLine.get(n);
				String lr = left_right.getValue();
				if (lr.equals("left")){
					s+="left ";
					i.lr = LeftRight.LEFT;
				}else if(lr.equals("right")){
					s+="right ";
					i.lr = LeftRight.RIGHT;
				}else{
					throw new AntBrainException ("please specify left or right");
				}
				n++;
				
				ValueToken state1 = (ValueToken) brainLine.get(n);
				int st1 = Integer.parseInt(state1.getValue());
				if (st1>length || st1<0){
					throw new AntBrainException("st1 should be between 0 and "+length);
				}else{
					s+=st1;
					i.st1 = st1;
				}
				n++;

			} else if (instr.equals("T_Move")) {
				s+="Move ";
				i.type = IType.MOVE;
				n++;
				
				ValueToken state1 = (ValueToken) brainLine.get(n);
				int st1 = Integer.parseInt(state1.getValue());
				if (st1>length || st1<0){
					throw new AntBrainException("st1 should be between 0 and "+length);
				}else{
					s+=st1+" ";
					i.st1 = st1;
				}
				n++;
				
				ValueToken state2 = (ValueToken) brainLine.get(n);
				int st2 = Integer.parseInt(state2.getValue());
				if (st2>length || st2<0){
					throw new AntBrainException("st2 should be between 0 and "+length);
				}else{
					s+=st2;
					i.st2 = st2;
				}
				n++;

			} else if (instr.equals("T_Flip")) {
				s+="Flip ";
				i.type = IType.FLIP;
				n++;
				
				ValueToken pToken = (ValueToken) brainLine.get(n);
				int p = Integer.parseInt(pToken.getValue());
				if (p<1){
					throw new AntBrainException("p should be more than 0");
				}else{
					s+=p+" ";
					i.p = p;
				}
				n++;
				
				ValueToken state1 = (ValueToken) brainLine.get(n);
				int st1 = Integer.parseInt(state1.getValue());
				if (st1>length || st1<0){
					throw new AntBrainException("st1 should be between 0 and "+length);
				}else{
					s+=st1+" ";
					i.st1 = st1;
				}
				n++;
				
				ValueToken state2 = (ValueToken) brainLine.get(n);
				int st2 = Integer.parseInt(state2.getValue());
				if (st2>length || st2<0){
					throw new AntBrainException("st2 should be between 0 and "+length);
				}else{
					s+=st2;
					i.st2 = st2;
				}
				n++;
				

			} else {
				throw new AntBrainException("unreckognised command at the beggining: " + instr);
			}
		} catch (IndexOutOfBoundsException e) {
			throw new AntBrainException("not enought tokens");
		}catch (NumberFormatException e){
			throw new AntBrainException ("unable to parse int");
		}catch (ClassCastException e){
			throw new AntBrainException ("wrong token at wrong place");
		}
		
		//
		//System.out.println(n +" / "+ brainLine.size());	
		if (n<brainLine.size() && ((Token)brainLine.get(n)).getName().equals("T_Semicolon")){
			
			n++;
			//System.out.println(n+" "+brainLine.size()+brainLine.get(n));
			
		
			for (int j =n; j<brainLine.size(); j++){
				Token com = brainLine.get(j);
				if(com.getName().equals("T_Comment")){
					s+= " "+((ValueToken)com).getValue();
					i.comment+=((ValueToken)com).getValue()+" ";
					
				}else{
					s+= " "+com;
					i.comment+=com+" ";
				}
				
			}
			
		}else if(n<brainLine.size()){
			throw new AntBrainException ("too many tokens! delete some tokens");
		}
		
		i.s = s;
		
		iList.add(i);

	}
	
	public ArrayList<Instruction> getInstructionList(){
		return iList;
	}
	
	public String getJSONBrain(){
		JSONArray dataArray = new JSONArray();
		dataArray.put(new JSONObject().put("size", iList.size()));
		JSONArray instr = new JSONArray();
		for (Instruction i : iList){
			instr.put(i);
		}
		dataArray.put(new JSONObject().put("instructions", instr));
		return dataArray.toString();
	}

}
