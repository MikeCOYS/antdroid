package antgame;
public class WorldException extends Exception {
    /**
	 * World exception is thrown when there is an error in the input world
	 * Errors can be: 
	 * height is different from number of lines, width of a row is different from specified width,
	 * unknown character in a line, height and width are not integers
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WorldException(String message) {
        super(message);
    }
}