package antgame;
public class AntBrainException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AntBrainException(String message) {
        super(message);
    }
}