package antgame;
import java.io.IOException;
//import java.util.ArrayList;


public class Match {
	
	private int round = 0;
	public AntBrain redAntBrain = null;
	public String redAntBrainName = "";
	public AntBrain blackAntBrain = null;
	public String blackAntBrainName = "";
	public World world = null;
	public String worldName = "";
	private int maxRounds = 0;
	public boolean redAntBrainSet, blackAntBrainSet, worldSet = false;
	
	public Match (int _maxRounds){
		maxRounds = _maxRounds;
		
	}
	
	public void addAntBrain(String antBrainName, String redOrBlack) throws IOException, MatchException{
		
		//read the ant-brain file and put its contents in the prog variable
		//MyFileReader fr = new MyFileReader("./files/"+antBrainName);
		String brain = "";// fr.getProgram();
		
		//create and run parser
		try {
			BrainParser bp = new BrainParser(brain, 10000);
			if (redOrBlack.equals("red")){
				this.redAntBrain = new AntBrain(bp.getInstructionList());
				this.redAntBrainName = antBrainName;
				redAntBrainSet = true;
			}else if (redOrBlack.equals("black")){
				this.blackAntBrain = new AntBrain(bp.getInstructionList());
				this.blackAntBrainName = antBrainName;
				blackAntBrainSet = true;

			}
			
		} catch (AntBrainException e) {
			throw new MatchException ("error with ant brain: "+e.getLocalizedMessage());
		}

	}

	public void addWorld(String wName) throws IOException, MatchException {
		
		//read the file and store its contents in the world variable
		//MyFileReader fr = new MyFileReader("./files/"+wName);
		String world = "";//fr.getProgram();
		
		try {
			WorldParser wp = new WorldParser(world);
			World w = new World(wp.getCells(), wp.width, wp.height, wp.rawWorld);
			this.world = w;
			this.worldSet = true;
			this.worldName = wName;
			
			//create new world
			//World myWorld = new World(world);	
			//this.world = myWorld;
			//this.worldSet = true;
			//this.worldName = wName;
			//ArrayList<String[]> cells = myWorld.getCells();
			
			//for (String[] cellRow : cells){
			//	for (String c : cellRow){
			//		System.out.print(c);
			//	}
			//	System.out.println();
			//}
		
		} catch (WorldException e) {
			throw new MatchException ("error with ant world: "+e.getLocalizedMessage());
		}


	}
	
	public void start(){
		//start the simulation;
	}
	
	
	public int getMaxRounds(){
		return maxRounds;
	}
	public int getRound(){
		return round;
	}
}
