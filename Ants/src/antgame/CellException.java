package antgame;
public class CellException extends Exception {
    /**
	 * Cell exception is thrown when there is an error while processing a cell
	 * Errors can be: 
	 * can't change food amount,
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CellException(String message) {
        super(message);
    }
}