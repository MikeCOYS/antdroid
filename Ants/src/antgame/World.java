package antgame;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class World {
	
	/**
	 * A class which represents the world
	 * 
	 */

	private ArrayList<String[]> cells;
	private int width;
	private int height;
	private String raw;
	
	public ArrayList<Cell> clearCells, rockyCells, redAntHillCells, blackAntHillCells, foodCells, allCells;
	public ArrayList<Cell> antHillCells;
	public ArrayList<Ant> redAnts, blackAnts;

	public World(ArrayList<String[]> c, int w, int h, String r) throws WorldException {
		this.cells = c;
		this.width = w;
		this.height = h;
		this.raw = r;
		
		redAnts = new ArrayList<Ant>();
		blackAnts = new ArrayList<Ant>();
		antHillCells = new ArrayList<Cell>();
		allCells = new ArrayList<Cell>();
		clearCells = new ArrayList<Cell>();
		rockyCells = new ArrayList<Cell>();
		redAntHillCells = new ArrayList<Cell>();
		blackAntHillCells = new ArrayList<Cell>();
		foodCells = new ArrayList<Cell>();
				
		construct();
		placeAnts();
	}

	private void construct() throws WorldException{
		for (int i =0; i<cells.size();i++){
			for (int j=0; j<cells.get(i).length; j++){
				String c = cells.get(i)[j];
				try{
					Cell cell = new Cell(c,j,i);
					switch (cell.getType()){
						case CLEAR:
							clearCells.add(cell);
							allCells.add(cell);
							break;
						case ROCKY:
							rockyCells.add(cell);
							allCells.add(cell);
							break;
						case REDANTHILL:
							redAntHillCells.add(cell);
							antHillCells.add(cell);
							allCells.add(cell);
							break;
						case BLACKANTHILL:
							blackAntHillCells.add(cell);
							antHillCells.add(cell);
							allCells.add(cell);
							break;
						case FOOD:
							foodCells.add(cell);
							allCells.add(cell);
							break;
						case NOT_SET:
							throw new WorldException ("Cell type is not set "+cell);
					}
					
				}catch(CellException ce){
					throw new WorldException ("Error creating cell "+c+" at "+j+";"+i);
				}
			}
		}
		
		System.out.println("Black ant hill cells: "+blackAntHillCells.size());
		System.out.println("Red ant hill cells: "+redAntHillCells.size());
//		System.out.println("Food cells: "+foodCells.size());
//		System.out.println("Rocky cells: "+rockyCells.size());
//		System.out.println("Clear cells: "+clearCells.size());

	}
	
	private void placeAnts(){
		int id = 0;
		for (Cell antHillCell : antHillCells){
			switch (antHillCell.getType()){
			case REDANTHILL:
				antHillCell.setRedAnt(id);
				Ant a = new Ant("red", id, antHillCell.getPosition()[0],antHillCell.getPosition()[1]);
				redAnts.add(a);
				break;
			case BLACKANTHILL:
				antHillCell.setBlackAnt(id);
				Ant a1 = new Ant("black", id, antHillCell.getPosition()[0],antHillCell.getPosition()[1]);
				blackAnts.add(a1);
				break;
			}
			id++;
		}
		
		//print world with ants
		for (int i=0; i<height; i++){
			for (int j=0; j<width; j++){
				Cell c = allCells.get(j+width*i);
				if(c.hasBlackAnt||c.hasRedAnt){
					System.out.print(c.getAntId()+" ");
				}else{
					System.out.print(". ");
				}
			}
			System.out.println();
		}
		//System.out.println(allCells);
	}
	
	private boolean isCorrect(){
		
		//to implement!
		return true;
	}
	
	public int getWidth(){
		return width;
	}
	public int getHeight(){
		return height;
	}

	public ArrayList<String[]> getCells() throws WorldException {
		if(isCorrect()){
			return cells;
		}else{
			throw new WorldException("World is incorrect");
		}
	}

	public String toString() {
		//override Object's toString method
		return raw;
	}
	public String getJSONWorld(){
		
		JSONArray dataArray = new JSONArray();
		dataArray.put(new JSONObject().put("width", width));
		dataArray.put(new JSONObject().put("height", height));
		
		JSONArray jsonA = new JSONArray();
		for (Cell c : rockyCells){
			JSONObject rockyCell = new JSONObject();
			rockyCell.put("x",c.getPosition()[0]);
			rockyCell.put("y",c.getPosition()[1]);
			
			jsonA.put(rockyCell);
			
		}
		dataArray.put(new JSONObject().put("rocks", jsonA));
		
		jsonA = new JSONArray();
		for (Cell c : redAntHillCells){
			JSONObject redAntHillCell = new JSONObject();
			redAntHillCell.put("x",c.getPosition()[0]);
			redAntHillCell.put("y",c.getPosition()[1]);
			
			jsonA.put(redAntHillCell);
		}
		dataArray.put(new JSONObject().put("redHills", jsonA));
		
		jsonA = new JSONArray();
		for (Cell c : blackAntHillCells){
			JSONObject blackAntHillCell = new JSONObject();
			blackAntHillCell.put("x",c.getPosition()[0]);
			blackAntHillCell.put("y",c.getPosition()[1]);
			
			jsonA.put(blackAntHillCell);
		}
		dataArray.put(new JSONObject().put("blackHills", jsonA));
		
		jsonA = new JSONArray();
		for (Cell c : foodCells){
			JSONObject foodCell = new JSONObject();
			foodCell.put("x",c.getPosition()[0]);
			foodCell.put("y",c.getPosition()[1]);
			foodCell.put("n",c.getFoodNo());

			jsonA.put(foodCell);
		}
		dataArray.put(new JSONObject().put("food", jsonA));
		
		jsonA = new JSONArray();
		for (Ant a : redAnts){
			JSONObject redAnt = new JSONObject();
			redAnt.put("x",a.x);
			redAnt.put("y",a.y);
			redAnt.put("id",a.id);

			jsonA.put(redAnt);
		}
		dataArray.put(new JSONObject().put("redAnts", jsonA));
		
		jsonA = new JSONArray();
		for (Ant a : blackAnts){
			JSONObject blackAnt = new JSONObject();
			blackAnt.put("x",a.x);
			blackAnt.put("y",a.y);
			blackAnt.put("id",a.id);

			jsonA.put(blackAnt);
		}
		dataArray.put(new JSONObject().put("blackAnts", jsonA));
		
		
		return dataArray.toString();
	}
//	public JSONObject getJSONWorld(){
//		
//		JSONObject dataArray = new JSONObject();
//		dataArray.put("width", width);
//		dataArray.put("height", height);
//		
//		JSONArray jsonA = new JSONArray();
//		for (Cell c : rockyCells){
//			JSONObject rockyCell = new JSONObject();
//			rockyCell.put("x",c.getPosition()[0]);
//			rockyCell.put("y",c.getPosition()[1]);
//			
//			jsonA.put(rockyCell);
//			
//		}
//		dataArray.put("rocks", jsonA);
//		
//		jsonA = new JSONArray();
//		for (Cell c : redAntHillCells){
//			jsonA.put(c.getPosition()[0]+";"+c.getPosition()[1]);
//		}
//		dataArray.put("redHills", jsonA);
//		
//		jsonA = new JSONArray();
//		for (Cell c : blackAntHillCells){
//			jsonA.put(c.getPosition()[0]+";"+c.getPosition()[1]);
//		}
//		dataArray.put("blackHills", jsonA);
//		
//		jsonA = new JSONArray();
//		for (Cell c : foodCells){
//			jsonA.put(c.getPosition()[0]+";"+c.getPosition()[1]+":"+c.getFoodNo());
//		}
//		dataArray.put("food", jsonA);
//		
//		jsonA = new JSONArray();
//		for (Ant a : redAnts){
//			jsonA.put(a.x+";"+a.y+":"+a.id);
//		}
//		dataArray.put("redAnts", jsonA);
//		
//		jsonA = new JSONArray();
//		for (Ant a : blackAnts){
//			jsonA.put(a.x+";"+a.y+":"+a.id);
//		}
//		dataArray.put("blackAnts", jsonA);
//		
//		
//		return dataArray;
//	}
}
