package antgame;
import java.util.ArrayList;

public class Lexer {
	
	/**
	 * The 1st component of a parser: lexer.
	 * Scans the input ant-brain represented as string and outputs a list of tokens.
	 * The ArrayList of tokens can then be passed to a parser to check if the brain is correct.
	 * 
	 */
	

	private String rawBrain;
	private ArrayList<ArrayList<Token>> tokenLines;

	public Lexer(String pr) throws AntBrainException {
		rawBrain = pr;
		tokenLines = new ArrayList<ArrayList<Token>>();
		//System.out.println("Initialised Lexer");
		parseBrain();
	}

	private void parseBrain() throws AntBrainException {

		//split the ant-brain into lines
		String[] lines = rawBrain.split("\\n");
		
		for (String line : lines){
			ArrayList<Token> tokenLine = new ArrayList<Token>();

			String[] commands = line.split("(\\s)+");
			for (int i = 0; i < commands.length; i++) {
				String s = commands[i];
				tokenLine.add(decideToken(s));
			}
			tokenLines.add(tokenLine);
		}

	}
	
	private Token decideToken(String s){
		Token returnToken;
		String flag = "(?i)";
		if (s.matches(flag+"Sense")) {
			returnToken = new Token("T_Sense");
		} else if (s.matches(flag+"Here|Ahead|LeftAhead|RightAhead")) {
			returnToken = new ValueToken("T_Sensedir", s.toLowerCase()); 
		} else if (s.matches(flag+"Marker|Foe|Friend|Home|FoeHome|FoeMarker|"
				+ "Rock|Food|FoeWithFood|FriendWithFood")) {
			returnToken = new ValueToken("T_Condition", s.toLowerCase());
		} else if (s.matches(flag+"Mark")) {
			returnToken = new Token("T_Mark");
		} else if (s.matches(flag+"Unmark")) {
			returnToken = new Token("T_Unmark");
		} else if (s.matches(flag+"PickUp")) {
			returnToken = new Token("T_Pickup");
		} else if (s.matches(flag+"Drop")) {
			returnToken = new Token("T_Drop");
		} else if (s.matches(flag+"Turn")) {
			returnToken = new Token("T_Turn");
		} else if (s.matches(flag+"Left|Right")) {
			returnToken = new ValueToken("T_Left_or_Right", s.toLowerCase());
		} else if (s.matches(flag+"Move")) {
			returnToken = new Token("T_Move");
		} else if (s.matches(flag+"Flip")) {
			returnToken = new Token("T_Flip");
		} else if (s.matches(flag+"[0-9]+")) {
			returnToken = new ValueToken("T_Number", s);
		} else if (s.matches(flag+";")){
			returnToken = new Token("T_Semicolon");
		}else {
			returnToken = new ValueToken("T_Comment", s);
		}
		return returnToken;
	}

	public ArrayList<ArrayList<Token>> getTokenLines() {
		return tokenLines;
	}

}
