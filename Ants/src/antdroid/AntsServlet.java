package antdroid;

import java.io.IOException;

import javax.servlet.http.*;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.appengine.api.datastore.DatastoreFailureException;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;

import antgame.*;

@SuppressWarnings("serial")
public class AntsServlet extends HttpServlet {

	private DatastoreService datastore;
	
	public AntsServlet(){
		datastore = DatastoreServiceFactory.getDatastoreService();

	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		resp.setContentType("text/plain");
		
		String getWhat = "";
		String noEntitiesMsg = "";
		
		if (req.getParameter("getWorldList") != null){
			
			getWhat = "World";
			noEntitiesMsg = new JSONObject().put("error", "no worlds").toString();
		
		}else if (req.getParameter("getAntBrainList") != null){
			getWhat = "AntBrain";
			noEntitiesMsg = new JSONObject().put("error", "no ant-brains").toString();

		}
		else if(req.getParameter("login") != null){
			resp.getWriter().println("ok");
			return;
		}
		
		if (!getWhat.equals("")){
			try {
				Query q = new Query(getWhat);
				PreparedQuery pq = datastore.prepare(q);
				FetchOptions fo = FetchOptions.Builder.withChunkSize(200);
				//FetchOptions.Builder.withLimit(limit)

				if(pq.countEntities(fo)==0){
					resp.getWriter().println(noEntitiesMsg);
				}else{
					JSONArray ja = new JSONArray();
					for (Entity result : pq.asIterable()) {
						if (result.getProperty("public").equals("true")){
							JSONObject json = new JSONObject();

							json.put("name",result.getKey().getName());// result.getProperty("name"));
							JSONArray jsonA = new JSONArray(((Text) result.getProperty("content")).getValue());							
							json.put("content", jsonA);

							ja.put(json);
						}
					}
					resp.getWriter().println(ja);
				}
				
			} catch (Exception e) {
				resp.getWriter().println(e);
			}
			
		}else{
			resp.getWriter().print("<html>hello "+req.getRemoteAddr()+". welcome to the ant-game. <a href=\"getAntBrainList\" getAnts></html>");
		}
			
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		// datastore.put(new Entity("World","world1"));

		resp.setContentType("text/plain");

		// resp.getWriter().println("Hello, world");
		// req.get

		// System.out.println((String) req.getParameter("a"));
		if (req.getParameter("a") != null) {
			try {
				Action a = Action.valueOf((String) req.getParameter("a"));
				switch (a) {
					
//					try {
//						Entity w = datastore.get(KeyFactory.createKey("World",
//								req.getParameter("worldID")));
//						// w.getProperty(propertyName)
//						resp.getWriter().println(w);
//					} catch (EntityNotFoundException e) {
//						resp.getWriter().println(
//								"500: no world " + req.getParameter("worldID"));
//					} catch (IllegalArgumentException e) {
//						resp.getWriter().println("501: no world id specified");
//
//						System.out.println("world id not specified");
//					}
//
//					System.out.println("getWorld");
//					break;
//				case getBrain:
//					
//					if (req.getParameter("antID") == null){
//						resp.getWriter().println(new JSONObject().put("error", "no antID specified"));
//						break;
//					}
//					try{
//						Entity brain = datastore.get(KeyFactory.createKey("AntBrain",req.getParameter("antID")));
//					}catch(EntityNotFoundException e) {
//						resp.getWriter().println(new JSONObject().put("error", "ant does not exist"));
//					}
//					break;
					
				case addWorld:
					
					try{
						if(req.getParameter("content") == null ||  req.getParameter("public")==null || req.getParameter("name")==null){
							resp.getWriter().println(new JSONObject().put("error", 701));
							//"701: world content, name or domain not specified"
							break;
						}
						//String content = req.getParameter("content");
						//Text c = new Text(content);
						String visible = req.getParameter("public");
						String worldName = req.getParameter("name");
						
						
							try {
								datastore.get(KeyFactory.createKey("World", worldName));
								
								JSONObject json = new JSONObject();
								json.put("status", "error");
								json.put("msg", "world already exists");
								
								resp.getWriter().println(json);
								
							} catch (EntityNotFoundException e) {
								// if world with such name does not exist
								
								String jsonWorld = "";
								
								try {
									WorldParser myWP = new WorldParser(req.getParameter("content"));
									World w = new World(myWP.getCells(), myWP.width, myWP.height, myWP.rawWorld);
									jsonWorld = w.getJSONWorld();
									
									Entity newWorld = new Entity("World",worldName);
									
									newWorld.setProperty("public", visible);
									newWorld.setProperty("content", new Text(jsonWorld));
									
									datastore.put(newWorld);
									
									JSONObject json = new JSONObject();
									json.put("status", "success");
									json.put("msg", "the world was added");
									
									resp.getWriter().println(json);
									
									
								} catch (WorldException e1) {								
									resp.getWriter().println(genResponse("error",e1.getMessage()));
								}
								//World myWorld = new World(req.getParameter("content"));
								
								
							}
						
													
						


					}catch(IllegalArgumentException e) {
						resp.getWriter().println("IllegalArgumentException");
					}catch(DatastoreFailureException e){
						resp.getWriter().println("100: datastore error");

					}
					break;

					
				case addAntBrain:
					try{
						if(req.getParameter("content") == null ||  req.getParameter("public")==null || req.getParameter("name")==null){
							resp.getWriter().println("601: ant content, name or domain not specified");
							break;
						}
						//String content = req.getParameter("content");
						//Text c = new Text(content);
						String visible = req.getParameter("public");
						String antName = req.getParameter("name");
						
						try {
							datastore.get(KeyFactory.createKey("AntBrain", antName));
							
							JSONObject json = new JSONObject();
							json.put("status", "error");
							json.put("msg", "ant-brain already exists");
							
							resp.getWriter().println(json);
							
						}catch(EntityNotFoundException e){
						
							
							try {
								Entity newAnt = new Entity("AntBrain",antName);
								BrainParser bp = new BrainParser(req.getParameter("content"),10000);
							
								
								newAnt.setProperty("public", visible);
								newAnt.setProperty("content", new Text(bp.getJSONBrain()));
								
								datastore.put(newAnt);
								
								JSONObject json = new JSONObject();
								json.put("status", "success");
								json.put("msg", "the ant-brain was added");
								
								resp.getWriter().println(json);
								
								
							} catch (AntBrainException e1) {								
								resp.getWriter().println(genResponse("error",e1.getMessage()));
							}
							
						}
						


					}catch(IllegalArgumentException e) {
						resp.getWriter().println("IllegalArgumentException");
					}catch(DatastoreFailureException e){
						resp.getWriter().println("100: datastore error");

					}
					break;

				}
				
			} catch (IllegalArgumentException e) {
				resp.getWriter().println(new JSONObject().put("error", "action does not exist"));
				System.out.println("wrong action argument");
			}
		}

		// Map<String, String[]> paramMap = req.getParameterMap();
		// Set<String> ks = paramMap.keySet();
		// for (String k : ks){
		// if (k.equals("getWorld")){
		//
		// Entity e;
		// try {
		// e = datastore.get(KeyFactory.createKey("World",
		// req.getParameter(k)));
		// resp.getWriter().println(e.getProperty("raw"));
		//
		// } catch (EntityNotFoundException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// resp.getWriter().println("404: world does not exist");
		//
		// }
		//
		//
		//
		// //resp.getWriter().println("3.3 #�#�#�. . . #�#�#");
		// }else if (k.equals("getGame")){
		// resp.getWriter().println("getGame"+req.getParameter(k));
		// }else if (k.equals("getAntBrain")){
		// resp.getWriter().println("getAntBrain"+req.getParameter(k));
		// }else if (k.equals("addWorlds")){
		//
		// String content1 = "3.3 #�#�#�# . # #�#�#";
		// String content2 = "4.4 . . . . #�#�#�# . # #�#�# . . . .";
		//
		// // Key worldKey = KeyFactory.createKey("World", "world1");
		// // System.out.println(worldKey);
		// Entity greeting = new Entity(KeyFactory.createKey("World",
		// "world1"));
		// greeting.setProperty("width", 20);
		// greeting.setProperty("height", 20);
		// greeting.setProperty("raw", content1);
		//
		// Entity greeting2 = new Entity(KeyFactory.createKey("World",
		// "world2"));
		// greeting2.setProperty("raw", content2);
		//
		// Entity greeting3 = new Entity(KeyFactory.createKey("World",
		// "world3"));
		// greeting3.setProperty("raw", "2.2 #�#�# .");
		// datastore.put(greeting3);
		//
		// // greeting.
		//
		// List<Entity> greetings = Arrays.asList(greeting, greeting2);
		// datastore.put(greetings);
		//
		// }else if(k.equals("listWorlds")){
		// try{
		// Query q = new Query("World");
		// PreparedQuery pq = datastore.prepare(q);
		//
		// for (Entity result : pq.asIterable()) {
		// resp.getWriter().println(result);
		//
		// }
		// }catch(Exception e){
		// resp.getWriter().println(e);
		// }
		//
		// //datastore.ge
		// }
		//
		// //resp.getWriter().print(k+": "+paramMap.get(k)[0]);
		// }

		// resp.getWriter().println(req.getParameterMap());
		// resp.getWriter().println(req.getRemoteHost()+" "+
		// req.getRemoteAddr()+" "+req.getServerName()+" "+req.getServletPath()+" "+req.getParameterMap());
	}
	
	private JSONObject genResponse(String status, String msg){
		JSONObject json = new JSONObject();
		json.put("status", status);
		json.put("msg", msg);
		
		return json;
	}
	
}
