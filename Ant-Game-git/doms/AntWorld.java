public class AntWorld {
	private Cell[][] antWorld;
	private int rows = 150;
	private int columns = 150;
	private int antHills, rocks, foods;

	//conjure up a (random?) antworld made up of a 2d hex array, populate it and whatnot.
	public AntWorld() {
		antWorld = new Cell[rows, columns];
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < columns; j++) {
                minefield[i][j] = new Cell();
            }
        }
        populate();
	}

	public void populate() {
		for(i = 0; i < rows; i++) {
			for(j = 0; j < columns; j++) {
					if(i == 0 || j == 0) {
						antWorld[i][j].setRock();
					}
					else if(i == 149 || j == 149) {
						antWorld[i][j].setRock();
					}										
				}
			}
		}
	}

	public void createRocks() {
		if(rocks <= 14) {
			//do stuff
		}
	}

	public void createFoodBlob() {
		if(foods <= 11) {
			//do stuff
		}
	}

	public void createAntHill() {
		if(antHills <= 2) {
			//do stuff
		}
	}
}