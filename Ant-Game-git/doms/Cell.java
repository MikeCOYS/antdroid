public class Cell {
	private boolean rock, antHill, ant;
	private int food;

	public Cell() {
		food = 0;
		rock = false;
		antHill = false;
		ant = false;
	}

	public void setRock() {
		rock = true;
	}

	public boolean getRock() {
		return rock;
	}

	public void setAntHill() {
		antHill = true;
	}

	public boolean getAntHill() {
		return antHill;
	}

	public int getFood() {
		return food;
	}

	public boolean getAnt() {
		if(ant == 1) {
			return true;
		}
		else {
			return false;
		}
	}

	public void appearAnt() {
		if(rock == false && antHill = false);
			if(ant == false) {
				ant = true;
		}
	}

	public void killAnt() {
		if(ant == true) {
			ant = false;
		}
	}

	public boolean isEmpty() {
		if(rock == false && antHill == false) {
			if(food == 0 && ant == false) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	public void createFoodCell() {
		if(rock == false && antHill = false) {
			food = 5;
		}
	}

	public boolean incFood() {
		if(rock == false && antHill = false) {
			if(food <= 5) {
				food = food + 1;
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	public void decFood() {
		if(rock == false && antHill = false) {
			if(food >= 1) {
				food = food - 1;
				return true;
			}
			else {
				return false;
			}
		}
		else { 
			return false;
		}
	}

	public String toString() {
		if(rock) {
			return "%";
		}
		else if(food >= 1) {
			return food + "";
		}
		else if(antHill) {
			return "&";
		}
		else if(ant) {
			return "A";
		}
		else{
			return " ";
		}
	}
}