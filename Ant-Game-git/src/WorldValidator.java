import java.util.*;

public class WorldValidator {
		int height, width;
		Cell[][] cells;
	
	public WorldValidator(){
	}
		

	public boolean validate(int width, int height, Cell[][] cells, int foodRectWidth, int antHillSide, int maxRocks) throws WorldException{
		//check if cells is actually 2D array of width/height
		//cells[0-height].length = width
		//cells.length = height;
		
		this.cells = cells;
		this.height = height;
		this.width = width;

		//Check 1st and last lines
		for (int i=0; i<width; i++){
			if (cells[i][0].getType().equals(CType.ROCKY)){
				throw new WorldException("Cell at the edge "+i+";0 is not rock!");
			}
			else if(cells[i][height-1].getType().equals(CType.ROCKY)) {
				throw new WorldException("Cell at the edge "+i+";"+(height-1)+" is not rock!");
			}
		}
		
		for (int j=0; j<height; j++){
			if (cells[0][j].getType().equals(CType.ROCKY)){
				throw new WorldException("Cell at the edge 0;"+j+" is not a rock!");
			}
			else if(cells[width-1][j].getType().equals(CType.ROCKY)){
				throw new WorldException("Cell at the edge "+(width-1)+";"+j+" is not rock!");
			}
		}

		
		if (!hasValidHill(CType.REDANTHILL))
		{
			throw new WorldException("Red anthill is invalid!");
		}
		
		if (!hasValidHill(CType.BLACKANTHILL))
		{
			throw new WorldException("Black anthill is invalid!");
		}

		if (!hasValidFood())
		{
			throw new WorldException("Invalid food blobs!");
		}

		if (!hasValidRocks())
		{
			throw new WorldException("Invalid rock formations!");
		}
		
		//World is valid!
		return true;
	}

	private boolean hasValidHill(CType colour)
	{
		char search = '+';
		if (colour.equals(CType.BLACKANTHILL))
		{
			search = '-';
		}
		int x = 0;
		int y = 0;
		boolean found = false;
		while (!found && y < height)
		{
			x = 3;
			while (!found && x < width)
			{
				if (match(x, y, search))
				{
					while (match(x - 1, y, search))
					{
						x -= 1;
					}
					found = true;
				} 
				else
				{
					x += 7;
				}
			}
			if (!found)
			{
				y++;
			}
		}
		// top left cell of anthill found at x, y
		// check adjacent cells first
		Position cell = new Position(x, y).getNextPosition(4);
		boolean okay = match(cell, '.');
		for (int i = 0; i < 6; i++)
		{
			int j = 0;
			while (okay && j++ < 7)
			{
				cell = cell.getNextPosition(i);
				okay = match(cell, '.');
			}
		}
		if (!okay)
		{
			return false;
		}
		// now checking the actual anthill
		ArrayList<Position> hill = checkHill(x, y);
		for (Position position : hill)
		{
			if (!match(position, search))
			{
				return false;
			}
		}
		return true;
	}

	private boolean isFood(int x, int y) {
		if(cells[x][y].getType().equals(CType.FOOD)) {
			return true;
		}
		return false;
	}

	private boolean hasValidFood() {
		int foodCellCount = 0;
		for (int y = 2; y < height - 2; y++)
		{
			for (int x = 2; x < width - 2; x++)
			{
				if (isFood(x, y))
				{
					foodCellCount++;
				}
			}
		}
		if (foodCellCount != 275)
		{
		return false;
		}
		//Correct number of food cells!
		boolean[][] food = new boolean[height][width];
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				food[y][x] = isFood(x, y);
			}
		}
		return validateFood(2, 2, food);
	}

	private boolean validateFood(int x, int y, boolean[][] food)
	{
		if (x < 0 || x >= width || y < 0 || y > height)
		{
			return false;
		}
		while (!food[y][x] && y < height - 1)
		{
			while (!food[y][x] && ++x < width - 1)
			{
			// do nothing
			}
			if (!food[y][x])
			{
				x = 2;
				y++;
			}
		}
		if (!food[y][x])
		{
			// no more unassigned food, so it's valid
			return true;
		}
		// otherwise a food cell has been found
		// This must be the originating part of the rectangle, so try each
		// arrangement of cells and then check rest of world
		int rows = food.length;
		for (int i = 0; i < 5; i++)
		{
			Position start = new Position(x, y);
			ArrayList<Position> foodBlob = checkRectangle(start, i);
			boolean possible = true;
			for (Position foods : foodBlob)
			{
				int xx = foods.getX();
				int yy = foods.getY();
				if (xx > width - 1 || yy > height - 1)
				{
					possible = false;
					break;
				}
				if (!food[yy][xx])
				{
					possible = false;
					break;
				}
			}
			if (possible)
			{
				// copy the current food array and remove this foodBlob from it
				// check if valid
				boolean[][] newFood = food.clone();
				for (int row = 0; row < rows; row++)
					{
					newFood[row] = food[row].clone();
					}
				for (Position foods : foodBlob)
				{
					newFood[foods.getY()][foods.getX()] = false;
				}
				if (validateFood(x + 1, y, newFood))
				{
				return true;
				}
			}
		}
		return false;
	}

	private boolean hasValidRocks()
	{
		int rockCount = 0;
		boolean[][] alreadyCounted = new boolean[height][width];
		for (int y = 2; y < height - 2; y++)
		{
			for (int x = 2; x < width - 2; x++)
			{
				if (cells[y][x].getType().equals(CType.ROCKY) && !alreadyCounted[y][x])
				{
					// New formation of rocks found
					rockCount++;
					alreadyCounted[y][x] = true;
					Position here = new Position(x, y);
					Stack<Position> toExplore = new Stack<Position>();
					for (int i = 0; i < 3; i++)
					{
						// Creates a stack of adjacent cells to be explored
						toExplore.add(here.getNextPosition(i));
					}
					while (!toExplore.isEmpty())
					{
						// get first cell to explore
						here = toExplore.pop();
						// if it's touching the boundaries of the world, not legal
						if (here.getX() < 2 || here.getX() >= width - 2 || here.getY() < 2 || here.getY() >= height - 2)
						{
							return false;
						}
						// if its already in a formation of rocks, ignore and move on
						else if (alreadyCounted[here.getY()][here.getX()])
						{
							continue;
						} 
						else
						{
							// otherwise mark it as part of a rock formation
							alreadyCounted[here.getY()][here.getX()] = true;
							// if it's a rock, get all of its adjacent cells and add
							// them to the toExplore stack
							if (cells[here.getY()][here.getX()].getSymbol() == '#')
							{
								for (int i = 0; i < 6; i++)
								{
									toExplore.add(here.getNextPosition(i));
								}
							}
							// If not, make sure its empty
							else
							{
								if (cells[here.getY()][here.getX()].getSymbol() != '.')
								{
									// A non-clear space has been found next a rock, invalid!
									return false;
								}
							}
						}
					}
				}
			}
		}
	if (rockCount != 14)
	{
		return false;
	}
	return true;
	}

	//Retrieve all cells that form a rectangle with the
	//given parameter cell as its origin 
	private ArrayList<Position> checkRectangle(Position cell, int orientation) 
	{
		if (orientation < 0 || orientation > 4)
		{
			return null;
		}
		ArrayList<Position> output = new ArrayList<Position>();
		output.add(cell);
		Position origin = cell;
		if (orientation == 4)
		{
			int i = 0;
			while (i++ < 4)
			{
				cell = cell.getNextPosition(2);
				output.add(cell);
				int j = 0;
				Position nextCell = cell;
				while (j++ < i)
				{
					nextCell = nextCell.getNextPosition(0);
					output.add(nextCell);
				}
			}
			while (--i > 0)
			{
				cell = cell.getNextPosition(1);
				output.add(cell);
				int j = 0;
				Position nextCell = cell;
				while (++j < i)
				{
					nextCell = nextCell.getNextPosition(0);
					output.add(nextCell);
				}
			}
		return output;
		} 
		else
		{
			int direction1 = 0;
			int direction2 = 0;
			switch (orientation)
			{
			case 0:
				direction1 = 2;
				direction2 = 2;
				break;
			case 1:
				direction1 = 1;
				direction2 = 1;
				break;
			case 2:
				direction1 = 1;
				direction2 = 2;
				break;
			case 3:
				direction1 = 2;
				direction2 = 1;
				break;
			}
			int j = 0;
			Position leftCell = origin;
			int direction = direction1;
			while (j < 5)
			{
				Position currCell = leftCell;
				output.add(currCell);
				for (int i = 0; i < 4; i++)
				{
					currCell = currCell.getNextPosition(0);
					output.add(currCell);
				}
				leftCell = leftCell.getNextPosition(direction);
				j++;
				direction = (direction == direction2) ? direction1 : direction2;
			}
		return output;
		}
	}

	//Checks to see if an anthill is in the valid configuration
	private ArrayList<Position> checkHill(int x, int y)
	{
		Position cell = new Position(x, y);
		ArrayList<Position> output = new ArrayList<Position>();
		int i = 0;
		while (i++ < 7)
		{
			Position nextCell = cell;
			int j = 7 + i;
			while (--j > 0)
			{
				output.add(nextCell);
				nextCell = nextCell.getNextPosition(0);
			}
			cell = cell.getNextPosition(2);
		}
		cell = cell.getNextPosition(5);
		while (--i > 0)
		{
			Position nextCell = cell;
			int j = 7 + i;
			while (--j > 0)
			{
				output.add(nextCell);
				nextCell = nextCell.getNextPosition(0);
			}
		cell = cell.getNextPosition(1);
		}
		return output;
	}	

	private boolean match(Position position, char character)
	{
		return match(position.getX(), position.getY(), character);
	}

	private boolean match(int x, int y, char character)
	{
		return cells[x][y].getSymbol() == character;
	}
}
