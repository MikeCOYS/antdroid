
public enum Condition {
	FRIEND, FOE, FRIEND_WITH_FOOD, FOE_WITH_FOOD,
	FOOD, ROCK, MARKER, FOE_MARKER, HOME, FOE_HOME,
	NOT_SET
	//	Friend
//	 	|	 	Foe
//	 	|	 	FriendWithFood
//	 	|	 	FoeWithFood
//	 	|	 	Food
//	 	|	 	Rock
//	 	|	 	Marker  i
//	 	|	 	FoeMarker
//	 	|	 	Home
//	 	|	 	FoeHome
}
