@SuppressWarnings("serial")
public class MatchException extends Exception {
    /**
	 * MatchException class is used to throw exceptions during the match.
	 * Reasons can be: invalid antbrain or invalid antworld, 
	 * to be continued
	 * 
	 */
	//private static final long serialVersionUID = 1L;

	public MatchException(String message) {
        super(message);
    }
}