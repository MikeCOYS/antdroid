import java.util.ArrayList;

public class World {
	
	/**
	 * A class which represents the world
	 * 
	 */  

	private ArrayList<String[]> cells;
	private int width;
	private int height;
	private String raw;
	
	public ArrayList<Cell> clearCells, rockyCells, redAntHillCells, blackAntHillCells, foodCells, allCells;
	public ArrayList<Cell> antHillCells;
	public ArrayList<Ant> redAnts, blackAnts;

	public World(ArrayList<String[]> c, int w, int h, String r) throws WorldException {
		//c is input array of characters
		this.cells = c;
		this.width = w;
		this.height = h;
		//r is the raw representation of the world
		this.raw = r;
		
		redAnts = new ArrayList<Ant>();
		blackAnts = new ArrayList<Ant>();
		antHillCells = new ArrayList<Cell>();
		allCells = new ArrayList<Cell>();
		clearCells = new ArrayList<Cell>();
		rockyCells = new ArrayList<Cell>();
		redAntHillCells = new ArrayList<Cell>();
		blackAntHillCells = new ArrayList<Cell>();
		foodCells = new ArrayList<Cell>();
				
		construct();
		placeAnts();
	}

	private void construct() throws WorldException{
		for (int i =0; i<cells.size();i++){
			for (int j=0; j<cells.get(i).length; j++){
				String c = cells.get(i)[j];
				try{
					Cell cell = new Cell(c,j,i);
					switch (cell.getType()){
						case CLEAR:
							clearCells.add(cell);
							allCells.add(cell);
							break;
						case ROCKY:
							rockyCells.add(cell);
							allCells.add(cell);
							break;
						case REDANTHILL:
							redAntHillCells.add(cell);
							antHillCells.add(cell);
							allCells.add(cell);
							break;
						case BLACKANTHILL:
							blackAntHillCells.add(cell);
							antHillCells.add(cell);
							allCells.add(cell);
							break;
						case FOOD:
							foodCells.add(cell);
							allCells.add(cell);
							break;
						case NOT_SET:
							throw new WorldException ("Cell type is not set "+cell);
					}
					
				}catch(CellException ce){
					throw new WorldException ("Error creating cell "+c+" at "+j+";"+i);
				}
			}
		}
		
		System.out.println("Black ant hill cells: "+blackAntHillCells.size());
		System.out.println("Red ant hill cells: "+redAntHillCells.size());
//		System.out.println("Food cells: "+foodCells.size());
//		System.out.println("Rocky cells: "+rockyCells.size());
//		System.out.println("Clear cells: "+clearCells.size());

	}
	
	private void placeAnts(){
		int id = 0;
		for (Cell antHillCell : antHillCells){
			switch (antHillCell.getType()){
			case REDANTHILL:
				antHillCell.setRedAnt(id);
				Ant a = new Ant("red", id, antHillCell.getPosition()[0],antHillCell.getPosition()[1]);
				redAnts.add(a);
				break;
			case BLACKANTHILL:
				antHillCell.setBlackAnt(id);
				Ant a1 = new Ant("black", id, antHillCell.getPosition()[0],antHillCell.getPosition()[1]);
				blackAnts.add(a1);
				break;
			}
			id++;
		}
		
		//printAnts();
		
		//System.out.println(allCells);
	}
	private void printAnts(){
		//print world with ants
				for (int i=0; i<height; i++){
					for (int j=0; j<width; j++){
						Cell c = allCells.get(j+width*i);
						if(c.hasBlackAnt||c.hasRedAnt){
							System.out.print(c.getAntId()+" ");
						}else{
							System.out.print(". ");
						}
					}
					System.out.println();
				}
	}
	
	public boolean isCorrect() throws WorldException{
						
		Cell cells[][] = new Cell[width][height];
		
		for (int i=0; i<height; i++){
			for (int j=0; j<width; j++){
				//System.out.println(allCells.get(j+height*i));
				cells[j][i] = allCells.get(j+height*i);
			}
		}
		
		
		boolean isValid = false;
		WorldValidator wv = new WorldValidator();
		try{
			isValid = wv.validate(width, height, cells, 5, 7, 14);
			System.out.println(isValid);
		}catch(WorldException we){
			throw new WorldException("Error validating the world.\n"+we.getMessage());
		}
		
		
		//return isValid;
		return true;
	}
	
	public int getWidth(){
		return width;
	}

	public int getHeight(){
		return height;
	}

	public ArrayList<String[]> getCells() throws WorldException {
		if(isCorrect()){
			return cells;
		}else{
			throw new WorldException("World is incorrect");
		}
	}

	public String toString() {
		//override Object's toString method
		return raw;
	}

}
