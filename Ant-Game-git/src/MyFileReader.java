import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class MyFileReader {
	
	/**
	 * FileReader class uses BufferedReader to read the requested file
	 * Stores the contents of the file in the program variable,
	 * which can be accessed using the getProgram() method
	 * Throws IOException when there is a problem,
	 * should be catched in a class which uses the FileReader or passed down
	 * 
	 */
	private String program; 
	
	public MyFileReader(String path) throws IOException{
		program = "";

		System.out.println("Reading the file " +getClass().getProtectionDomain().getCodeSource().getLocation() + path);
		
		
		BufferedReader bf = null;
		
		try{
			bf = new BufferedReader(new FileReader(path));
			String c;
			while ((c = bf.readLine()) != null ){
				program = program + c +"\n";
			}
		} finally {
			if (bf != null) {
				bf.close();
			}
		}
		
	}
	public String getProgram(){
		return program;
	}
}
