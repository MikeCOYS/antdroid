public class Position {
	private int x, y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

//<<<<<<< HEAD
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Position getNextPosition(int direction)
	{
		switch (direction)
		{
		case 0:
			return new Position(x + 1, y);
		case 1:
			return new Position(x + ((y % 2 == 0) ? 0 : 1), y + 1);
		case 2:
			return new Position(x + ((y % 2 == 0) ? -1 : 0), y + 1);
		case 3:
			return new Position(x - 1, y);
		case 4:
			return new Position(x + ((y % 2 == 0) ? -1 : 0), y - 1);
		case 5:
			return new Position(x + ((y % 2 == 0) ? 0 : 1), y - 1);
		}
		return this;
	}
}
