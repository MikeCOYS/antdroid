public class ValueToken extends Token{
	
	/**
	 * ValueToken class is an extension to a simple token class,
	 * used to store a token and its value
	 * e.g. Numbers, Left or Right and conditions, sense_dir and comment
	 */
	
	private String value;
	
	public ValueToken(String _name, String _value) {
		super(_name);
		value = _value;
	}
	public String getValue(){
		return value;
	}
	
	public String toString(){
		return getName()+"("+value+")";
	}
	
}

