import java.util.ArrayList;


public class AntBrain {
	
	/**
	 * The class to work with ant-brains. 
	 * To implement.
	 * 
	 */
	
	private ArrayList<Instruction> instructions;
	
	public AntBrain(ArrayList<Instruction> i){
		//System.out.println("new ant added");
		instructions = i;
	}
	public ArrayList<Instruction> getInstructions(){
		return instructions;
	}


}