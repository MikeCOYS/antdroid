public class Cell {
	public boolean hasRedAnt, hasBlackAnt;
	private int foodNo, antId = 0;
	private int[] position;
	private Position newPosition;
	private CType cType = CType.NOT_SET;

	/**
	* Constructor for cell.
	* @param cellType = the ASCII character that represents the cell on the map.
	* @param x = the cell's x co-ordinate.
	* @param y = the cell's y co-ordinate.
	* These co-ordinates are stored then stored in the position array, and the 
	* cell's type declared.
	 * @throws CellException 
	*/
	public Cell(String cellType, int x, int y) throws CellException {
		position = new int[2];
		position[0] = x;
		position[1] = y;
		newPosition = new Position(x,y);
		
		if(cellType.equals("#")) {
			cType = CType.ROCKY;
		}else if(cellType.equals("+")) {
			cType = CType.REDANTHILL;
		}else if(cellType.equals("-")) {
			cType = CType.BLACKANTHILL;
		}else if(cellType.equals(".")) {
			cType = CType.CLEAR;
		}else if(cellType.matches("\\d*")){
			cType = CType.FOOD;
			try{
				this.foodNo = Integer.parseInt(cellType);
			}catch(NumberFormatException nfe){
				throw new CellException("Cannot parse food number: "+cellType);
			}
		}else{
			throw new CellException("Cell type not recognised: "+cellType);
		}
		hasBlackAnt = false;
		hasRedAnt = false;
		
	}

	/**
	* Secondary constructor for cell if extra int representing foodNo is input.
	* @param cellType = the ASCII character that represents the cell on the map.
	* @param foodNo = how much food will be stored in this cell.
	* @param x = the cell's x co-ordinate.
	* @param y = the cell's y co-ordinate.
	* Same as before, except food is stored in the foodNo variable.
	 * @throws CellException 
	*/
//	public Cell(String cellType, int foodNo, int x, int y) throws CellException {
//		this("food",x,y);
//		if(cellType.equals("food")) {
//			this.foodNo = foodNo;
//		}else{
//			throw new CellException ("Trying to construct a food cell which is not food");
//		}
//	}

	public String toString(){
		return "c"+position[0] + ";"+position[1];
	}
	
	/**
	* Returns the cell's type.
	*/	
	public CType getType() {
		return cType;
	}
	
	/**
	* Returns amount of food stored in foodNo.
	*/
	public int getFoodNo() {
		return foodNo;
	}

	/**
	* Increments the amount of food (if a food cell) by 1.
	 * @throws CellException 
	*/	
	public void incFood() throws CellException{
		if(cType.equals(CType.FOOD)) {
			//if(foodNo < 5) {
			foodNo = foodNo + 1;
			
		}else{
			throw new CellException("Cell must contain food to increase it");
		}
	}

	/**
	* Decrements the amount of food (if a food cell) by 1.
	 * @throws CellException 
	*/		
	public void decFood() throws CellException{
		if(cType.equals(CType.FOOD)) {
			if(foodNo > 0) {
				foodNo = foodNo - 1;
			}else{
				throw new CellException("Food must be positive to decrease");
			}
		}else{
			throw new CellException("Cell must contain food to decrease it");
		}
	}
	
	/**
	* Returns the ID of the ant in this cell.
	*/	
	public int getAntId() {
		return antId;
	}
	

	/**
	* Sets the cell to contain a black ant if cell is clear.
	*/		
	public void setBlackAnt(int _id) {
		if(!cType.equals(CType.ROCKY)) {
			hasBlackAnt = true;
		}
		antId = _id;
	}

	/**
	* Sets the cell to contain a red ant if cell is clear.
	*/			
	public void setRedAnt(int _id) {
		if(!cType.equals(CType.ROCKY)) {
			hasRedAnt = true;
		}
		antId = _id;
	}

	/**
	* Clears any ants in this cell.
	*/			
	public void clearAnt() {
		hasRedAnt = false;
		hasBlackAnt = false;
	}

	/**
	* Returns the array in which the cell's position is stored.
	*/			
	public int[] getPosition() {
		return position;
	}
	
	public Position getNewPosition() {
		return newPosition;
	}

	public char getSymbol() {
		if(cType.equals(CType.ROCKY)) {
			return '#';
		}
		else if(cType.equals(CType.CLEAR)) {
			return '.';
		}
		else if(cType.equals(CType.REDANTHILL)) {
			return '+';
		}
		else if(cType.equals(CType.BLACKANTHILL)) {
			return '-';
		}
		return ' ';
	}
	
}