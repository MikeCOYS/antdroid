
public class Ant {
	
	public int x, y = -1;
	public int id = -1;
	public AntColour antColour = AntColour.NOT_SET;
	
	public Ant(String colour, int _id, int _x, int _y){
		if(colour.equals("red")){
			antColour = AntColour.RED;
		}else if(colour.equals("black")){
			antColour = AntColour.BLACK;
		}
		
		x = _x;
		y = _y;
		id = _id;
	}
}
